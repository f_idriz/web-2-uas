<!DOCTYPE html>
<html>
<head>
    <title>Form Pendaftaran Anggota</title>
    <!-- Load file CSS Bootstrap online -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

</head>
<body>
<div class="container mt-5">
    <?php
    //Include file koneksi, untuk koneksikan ke database
    include "koneksi.php";

    //Fungsi untuk mencegah inputan karakter yang tidak sesuai
    function input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    //Cek apakah ada kiriman form dari method post
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $username=input($_POST["username"]); //mengambil variabel username dari input
        $nama=input($_POST["nama"]); //mengambil variabel nama dari input
        $alamat=input($_POST["alamat"]); //mengambil variabel alamat dari input
        $email=input($_POST["email"]); //mengambil variabel email dari input
        $no_hp=input($_POST["no_hp"]); //mengambil variabel no_hp dari input
        $id_hobi=input($_POST["id_hobi"]); //mengambil variabel id_hobi dari input

        //Query input menginput data kedalam tabel anggota
        $sql="insert into anggota (username,nama,alamat,email,no_hp,id_hobi) values ('$username','$nama','$alamat','$email','$no_hp','$id_hobi')";

        //Mengeksekusi/menjalankan query diatas
        $hasil=mysqli_query($kon, $sql);

        //Kondisi apakah berhasil atau tidak dalam mengeksekusi query diatas
        if ($hasil) {
            header("Location:index.php");
        }
        else {
            echo "<div class='alert alert-danger'> Data Gagal disimpan.</div>";

        }

    }
    ?>
    <h2>Input Data</h2>


    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
        <div class="form-group">
            <label>Username:</label>
            <input type="text" name="username" class="form-control" placeholder="Masukan Username" required />

        </div>
        <div class="form-group">
            <label>Nama:</label>
            <input type="text" name="nama" class="form-control" placeholder="Masukan Nama" required/>

        </div>
        <div class="form-group">
            <label>Alamat:</label>
            <textarea name="alamat" class="form-control" rows="5"placeholder="Masukan Alamat" required></textarea>

        </div>
        <div class="form-group">
            <label>Hobi:</label>
            <select class="form-control" name="id_hobi">
              <?php
                $hobi="select * from hobi";
                $a=mysqli_query($kon, $hobi);
                while ($h = mysqli_fetch_array($a)) {
                    ?>
                    <option value="<?php echo $h["id_hobi"]; ?>"><?php echo $h["nama_hobi"]; ?></option>
                    <?php
                }
              ?>
            </select>

        </div>
        <div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" class="form-control" placeholder="Masukan Email" required/>
        </div>
        <div class="form-group">
            <label>No HP:</label>
            <input type="text" name="no_hp" class="form-control" placeholder="Masukan No HP" required/>
        </div>

        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
</html>
