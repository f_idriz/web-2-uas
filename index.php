<!DOCTYPE html>
<html>
<head>
  <title>List Anggota</title>
  <!-- Load file CSS Bootstrap online -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<div class="container mt-5">
    <br>
    <h4>Anggota</h4>
<?php

    include "koneksi.php";

    //Cek apakah ada nilai dari method GET dengan nama id_anggota
    if (isset($_GET['id_anggota'])) {
        $id_anggota=htmlspecialchars($_GET["id_anggota"]);

        $sql="delete from anggota where id_anggota='$id_anggota' ";
        $hasil=mysqli_query($kon,$sql);

        //Kondisi apakah berhasil atau tidak
            if ($hasil) {
                header("Location:index.php");

            }
            else {
                echo "<div class='alert alert-danger'> Data Gagal dihapus.</div>";

            }
        }
?>


    <table class="table table-bordered table-hover">
        <br>
        <thead>
        <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">Username</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Alamat</th>
            <th style="text-align: center;">Hobi</th>
            <th style="text-align: center;">Email</th>
            <th style="text-align: center;">No HP</th>
            <th style="text-align: center;" colspan='2'>Aksi</th>

        </tr>
        </thead>
        <?php
          include "koneksi.php";
          $sql="select anggota.id_anggota, anggota.username, anggota.nama, anggota.alamat, hobi.nama_hobi, anggota.email, anggota.no_hp from anggota inner join hobi on anggota.id_hobi = hobi.id_hobi";

          $hasil=mysqli_query($kon, $sql);
          $no=0;
          while ($data = mysqli_fetch_array($hasil)) {
              $no++;
              ?>
              <tbody>
              <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $data["username"]; ?></td>
                  <td><?php echo $data["nama"];   ?></td>
                  <td><?php echo $data["alamat"];   ?></td>
                  <td><?php echo $data["nama_hobi"];   ?></td>
                  <td><?php echo $data["email"];   ?></td>
                  <td><?php echo $data["no_hp"];   ?></td>
                  <td>
                      <a href="update.php?id_anggota=<?php echo htmlspecialchars($data['id_anggota']); ?>" class="btn btn-warning" role="button">Update</a>
                  </td>
                  <td>
                      <a onclick="return confirm('hapus data?')" href="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>?id_anggota=<?php echo $data['id_anggota']; ?>" class="btn btn-danger" role="button">Delete</a>
                  </td>
              </tr>
              </tbody>
              <?php
          }
        ?>
    </table>
    <a href="create.php" class="btn btn-primary" role="button">Tambah Data</a>

</div>
</body>
</html>
